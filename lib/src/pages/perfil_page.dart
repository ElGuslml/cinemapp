import 'package:cinemapp2/src/models/boletos_model.dart';
import 'package:cinemapp2/src/providers/shared_provider.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:flutter/material.dart';

class PerfilPage extends StatefulWidget {
  PerfilPage({Key key}) : super(key: key);

  @override
  _PerfilPageState createState() => _PerfilPageState();
}

class _PerfilPageState extends State<PerfilPage> {
  PreferenciasUsuario _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 450) {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.pushNamed(context, 'home');
            },
          ),
          backgroundColor: Color(0xFF212121),
          title: Text('CinemApp | Perfil'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.person), onPressed: () {})
          ],
        ),
        body: Container(
          child: ListView(
            children: <Widget>[
              _datosUsr(),
              _comprasHechasRes(),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.pushNamed(context, 'home');
            },
          ),
          backgroundColor: Color(0xFF212121),
          title: Text('CinemApp | Perfil'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.person), onPressed: () {})
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _datosUsr(),
              _comprasHechas(),
            ],
          ),
        ),
      );
    }
  }

  Widget _crearTarjeta(BoletosModel boleto) {
    return Container(
      padding: EdgeInsets.all(16),
      height: 600,
      width: 250,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          FadeInImage(
            fit: BoxFit.cover,
            image: NetworkImage(boleto.coverURL),
            placeholder: AssetImage('assets/img/no-image.jpg'),
          ),
          Container(
            color: Color.fromRGBO(0, 0, 0, 0.6),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                boleto.title,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.horario,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.boletos,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.asientos,
                style: TextStyle(color: Colors.white),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _comprasHechas() {
    SharedProvider shr = new SharedProvider();
    List<BoletosModel> listBoletos = shr.setBoletos();
    List<Widget> lista = new List();

    if (listBoletos != null) {
      listBoletos.forEach((item) {
        BoletosModel boleto = item;
        Widget wid = _crearTarjeta(boleto);
        lista.add(wid);
      });
    }

    return Container(
      width: MediaQuery.of(context).size.width * 0.66,
      padding: EdgeInsets.all(16),
      child: Card(
        child: GridView.count(
            childAspectRatio: 0.8,
            crossAxisCount: 2,
            shrinkWrap: true,
            children: lista),
      ),
    );
  }

  Widget _crearTarjetaRes(BoletosModel boleto) {
    return Container(
      padding: EdgeInsets.all(16),
      height: 400,
      width: 280,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          FadeInImage(
            fit: BoxFit.cover,
            image: NetworkImage(boleto.coverURL),
            placeholder: AssetImage('assets/img/no-image.jpg'),
          ),
          Container(
            color: Color.fromRGBO(0, 0, 0, 0.6),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                boleto.title,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.horario,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.boletos,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                boleto.asientos,
                style: TextStyle(color: Colors.white),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _comprasHechasRes() {
    SharedProvider shr = new SharedProvider();
    List<BoletosModel> listBoletos = shr.setBoletos();
    List<Widget> lista = new List();

    if (listBoletos != null) {
      listBoletos.forEach((item) {
        BoletosModel boleto = item;
        Widget wid = _crearTarjetaRes(boleto);
        lista.add(wid);
      });
    }

    return Container(
      width: MediaQuery.of(context).size.width * 0.66,
      padding: EdgeInsets.all(16),
      child: Card(
        child: GridView.count(
            childAspectRatio: 0.8,
            crossAxisCount: 2,
            shrinkWrap: true,
            children: lista),
      ),
    );
  }

  Widget _datosUsr() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.3,
      padding: EdgeInsets.all(16),
      child: Card(
        child: Column(
          children: <Widget>[
            Text(
              'Datos personales',
              style: Theme.of(context).textTheme.headline3,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            Text('Username', style: Theme.of(context).textTheme.headline4),
            Text(_prefs.username),
            Text('Nombre', style: Theme.of(context).textTheme.headline4),
            Text(_prefs.nombre),
            RaisedButton(
              onPressed: () {
                _prefs.isLoggedIn = false;
                Navigator.of(context).pushNamed('home');
              },
              child: Text('Cerrar Sesión'),
            )
          ],
        ),
      ),
    );
  }
}
