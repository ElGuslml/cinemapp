import 'package:cinemapp2/src/models/actores_model.dart';
import 'package:cinemapp2/src/models/pelicula_asiento_model.dart';
import 'package:cinemapp2/src/models/pelicula_model.dart';
import 'package:cinemapp2/src/models/videos_model.dart';
import 'package:cinemapp2/src/providers/peliculas_provider.dart';
import 'package:cinemapp2/src/providers/videos_provider.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class PeliculaDetalle extends StatefulWidget {
  PeliculaDetalle({Key key}) : super(key: key);

  @override
  _PeliculaDetalleState createState() => _PeliculaDetalleState();
}

class _PeliculaDetalleState extends State<PeliculaDetalle> {
  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;

    if (MediaQuery.of(context).size.width < 450) {
      return Scaffold(
        body: CustomScrollView(
          shrinkWrap: true,
          slivers: <Widget>[
            _crearAppBar(pelicula, true),
            SliverList(
                delegate: SliverChildListDelegate([
              _crearInfoPeli(pelicula, true),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Horario',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: GridView.count(
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    childAspectRatio: 1.5,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    children: _crearHorarios(context, pelicula)),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Sinopsis',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  pelicula.overview,
                  style: TextStyle(fontSize: 16),
                  textAlign: TextAlign.justify,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Trailer',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              _builderTrailerResp(pelicula),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Casting',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              _crearCastingRes(pelicula)
            ]))
          ],
        ),
      );
    } else {
      return Scaffold(
        body: CustomScrollView(
          shrinkWrap: true,
          slivers: <Widget>[
            _crearAppBar(pelicula, false),
            SliverList(
                delegate: SliverChildListDelegate([
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 30.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _posterTitulo(context, pelicula),
                    _descripcion(context, pelicula)
                  ],
                ),
              )
            ]))
          ],
        ),
      );
    }
  }

  Widget _crearAppBar(Pelicula pelicula, bool responsive) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.black,
      expandedHeight: 400,
      floating: false,
      pinned: false,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(responsive ? '' : pelicula.title),
        background: FadeInImage(
          image: NetworkImage(
              responsive ? pelicula.getCoverImg() : pelicula.getPosterImg()),
          placeholder: AssetImage('assets/img/no-image.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

Widget _posterTitulo(BuildContext context, Pelicula pelicula) {
  return Container(
    width: 500,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Image(
              image: NetworkImage(pelicula.getCoverImg()),
              height: 600.0,
            )),
        _crearInfoPeli(pelicula, false),
        Text(
          'Horarios',
          style: Theme.of(context).textTheme.headline3,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 4,
              childAspectRatio: 1.5,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              children: _crearHorarios(context, pelicula)),
        )
      ],
    ),
  );
}

Widget _crearInfoPeli(Pelicula pelicula, bool responsive) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 26),
    width: double.infinity,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
          child: Text(
            '130 min',
            style: TextStyle(color: Colors.white),
          ),
          color: Color(0xFF212121),
          onPressed: () {},
        ),
        RaisedButton(
          child: Text(
            'B-15',
            style: TextStyle(color: Colors.white),
          ),
          color: Color(0xFF43A047),
          onPressed: () {},
        ),
        RaisedButton(
          child: Row(
            children: <Widget>[
              Icon(
                Icons.star,
                color: Colors.white,
              ),
              Text(
                pelicula.voteAverage.toString(),
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          color: Color(0xFFfbc02d),
          onPressed: () {},
        )
      ],
    ),
  );
}

List<Widget> _crearHorarios(context, Pelicula pelicula) {
  List<Widget> lista = new List();
  List<String> horarios = [
    '12:00',
    '12:30',
    '13:00',
    '13:45',
    '14:00',
    '16:00',
    '17:30',
    '19:45',
    '20:00',
    '20:30',
    '21:00',
  ];

  horarios.forEach((item) {
    lista.add(RaisedButton(
      child: Text(
        item,
        style: TextStyle(color: Colors.white),
      ),
      onPressed: () {
        PeliculaAsiento datos = new PeliculaAsiento();
        datos.horario = item;
        datos.id = pelicula.id;
        datos.originalLanguage = pelicula.originalLanguage;
        datos.originalTitle = pelicula.originalTitle;
        datos.overview = pelicula.overview;
        datos.popularity = pelicula.popularity;
        datos.posterPath = pelicula.posterPath;
        datos.releaseDate = pelicula.releaseDate;
        datos.title = pelicula.title;
        datos.video = pelicula.video;
        datos.voteAverage = pelicula.voteAverage;
        datos.voteCount = pelicula.voteCount;
        Navigator.of(context).pushNamed('asientos', arguments: datos);
      },
      color: Color(0xFF212121),
    ));
  });

  return lista;
  //return FlatButton(onPressed: () {}, child: Text('13:00'));
}

Widget _builderTrailer(Pelicula pelicula) {
  final videosProvider = new VideosProvider();
  return FutureBuilder(
    future: videosProvider.getVideos(pelicula.id),
    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
      if (snapshot.hasData) {
        print(snapshot.data);
        return _trailerVideo(snapshot.data);
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
  );
}

Widget _builderTrailerResp(Pelicula pelicula) {
  final videosProvider = new VideosProvider();
  return FutureBuilder(
    future: videosProvider.getVideos(pelicula.id),
    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
      if (snapshot.hasData) {
        print(snapshot.data);
        return _trailerVideoRes(snapshot.data);
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
  );
}

Widget _trailerVideoRes(List<Video> videos) {
  try {
    YoutubePlayerController controller = YoutubePlayerController(
        initialVideoId: videos[0].key,
        flags: YoutubePlayerFlags(
          autoPlay: true,
          mute: false,
        ));

    if (videos.length > 0) {
      return Container(
        width: 300,
        height: 200,
        child: YoutubePlayer(
          controller: controller,
          showVideoProgressIndicator: true,
          onReady: () {
            print('Player ready');
          },
        ),
      );
    } else {
      return Text('No tiene video');
    }
  } catch (e) {
    print(e);
  }
}

Widget _trailerVideo(List<Video> videos) {
  try {
    YoutubePlayerController controller = YoutubePlayerController(
        initialVideoId: videos[0].key,
        flags: YoutubePlayerFlags(
          autoPlay: true,
          mute: false,
        ));

    if (videos.length > 0) {
      return Container(
        width: 700,
        height: 400,
        child: YoutubePlayer(
          controller: controller,
          showVideoProgressIndicator: true,
          onReady: () {
            print('Player ready');
          },
        ),
      );
    } else {
      return Text('No tiene video');
    }
  } catch (e) {
    print(e);
  }
}

Widget _crearCasting(Pelicula pelicula) {
  final peliProvider = new PeliculasProvider();

  return FutureBuilder(
    future: peliProvider.getCast(pelicula.id.toString()),
    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
      if (snapshot.hasData) {
        return _crearActoresPageView(snapshot.data);
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
  );
}

Widget _crearCastingRes(Pelicula pelicula) {
  final peliProvider = new PeliculasProvider();

  return FutureBuilder(
    future: peliProvider.getCast(pelicula.id.toString()),
    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
      if (snapshot.hasData) {
        return _crearActoresPageViewRes(snapshot.data);
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
  );
}

Widget _crearActoresPageViewRes(List<Actor> actores) {
  return SizedBox(
    height: 300.0,
    child: PageView.builder(
      pageSnapping: false,
      controller: PageController(viewportFraction: 0.5, initialPage: 1),
      itemCount: actores.length,
      itemBuilder: (context, i) => _tarjetaActorRes(actores[i]),
    ),
  );
}

Widget _tarjetaActorRes(Actor actor) {
  return Container(
    child: Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: FadeInImage(
            image: NetworkImage(actor.getCoverImg()),
            placeholder: AssetImage('assets/img/no-image.jpg'),
            height: 250,
            fit: BoxFit.cover,
          ),
        ),
        Text(
          actor.name,
          overflow: TextOverflow.ellipsis,
        )
      ],
    ),
  );
}

Widget _crearActoresPageView(List<Actor> actores) {
  return SizedBox(
    height: 400.0,
    child: PageView.builder(
      pageSnapping: false,
      controller: PageController(viewportFraction: 0.2, initialPage: 1),
      itemCount: actores.length,
      itemBuilder: (context, i) => _tarjetaActor(actores[i]),
    ),
  );
}

Widget _tarjetaActor(Actor actor) {
  return Container(
    child: Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: FadeInImage(
            image: NetworkImage(actor.getCoverImg()),
            placeholder: AssetImage('assets/img/no-image.jpg'),
            height: 350,
            fit: BoxFit.cover,
          ),
        ),
        Text(
          actor.name,
          overflow: TextOverflow.ellipsis,
        )
      ],
    ),
  );
}

Widget _descripcion(BuildContext context, Pelicula pelicula) {
  return Flexible(
    child: Column(
      children: <Widget>[
        Text(
          'Sinopsis',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          pelicula.overview,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          'Trailer',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(
          height: 20,
        ),
        _builderTrailer(pelicula),
        SizedBox(
          height: 30,
        ),
        Text(
          'Casting',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(
          height: 20,
        ),
        _crearCasting(pelicula),
      ],
    ),
  );
}
