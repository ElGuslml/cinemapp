import 'dart:js_util';

import 'package:cinemapp2/src/icons/cinema_icons_icons.dart';
import 'package:cinemapp2/src/models/pelicula_asiento_model.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:flutter/material.dart';

class AsientosPage extends StatefulWidget {
  AsientosPage({Key key}) : super(key: key);

  @override
  _AsientosPageState createState() => _AsientosPageState();
}

class _AsientosPageState extends State<AsientosPage> {
  String _usuario = "";
  String _password = "";

  List<String> asientos = new List();

  Color _iconColor = Colors.red;
  Color _iconColor1 = Colors.red;
  Color _iconColor2 = Colors.red;
  Color _iconColor3 = Colors.red;
  Color _iconColor4 = Colors.red;
  Color _iconColor5 = Colors.red;
  Color _iconColor6 = Colors.red;
  Color _iconColor7 = Colors.red;
  Color _iconColor8 = Colors.red;
  Color _iconColor9 = Colors.red;
  Color _iconColor10 = Colors.red;
  Color _iconColor11 = Colors.red;
  Color _iconColor12 = Colors.red;
  Color _iconColor13 = Colors.red;
  Color _iconColor14 = Colors.red;
  Color _iconColor15 = Colors.red;
  Color _iconColor16 = Colors.red;
  Color _iconColor17 = Colors.red;
  Color _iconColor18 = Colors.red;
  Color _iconColor19 = Colors.red;
  Color _iconColor20 = Colors.red;
  Color _iconColor21 = Colors.red;
  Color _iconColor22 = Colors.red;
  Color _iconColor23 = Colors.red;
  Color _iconColor24 = Colors.red;
  Color _iconColor25 = Colors.red;
  Color _iconColor26 = Colors.red;
  Color _iconColor27 = Colors.red;
  Color _iconColor28 = Colors.red;
  Color _iconColor29 = Colors.red;
  Color _iconColor30 = Colors.red;
  Color _iconColor31 = Colors.red;
  Color _iconColor32 = Colors.red;
  Color _iconColor33 = Colors.red;
  Color _iconColor34 = Colors.red;
  Color _iconColor35 = Colors.red;
  Color _iconColor36 = Colors.red;
  Color _iconColor37 = Colors.red;
  Color _iconColor38 = Colors.red;
  Color _iconColor39 = Colors.red;
  Color _iconColor40 = Colors.red;
  Color _iconColor41 = Colors.red;
  Color _iconColor42 = Colors.red;
  Color _iconColor43 = Colors.red;
  Color _iconColor44 = Colors.red;
  Color _iconColor45 = Colors.red;
  Color _iconColor46 = Colors.red;
  Color _iconColor47 = Colors.red;
  Color _iconColor48 = Colors.red;
  Color _iconColor49 = Colors.red;
  Color _iconColor50 = Colors.red;
  Color _iconColor51 = Colors.red;
  Color _iconColor52 = Colors.red;
  Color _iconColor53 = Colors.red;
  Color _iconColor54 = Colors.red;
  Color _iconColor55 = Colors.red;
  Color _iconColor56 = Colors.red;
  Color _iconColor57 = Colors.red;
  Color _iconColor58 = Colors.red;
  Color _iconColor59 = Colors.red;

  bool _isSel = false;
  bool _isSel1 = false;
  bool _isSel2 = false;
  bool _isSel3 = false;
  bool _isSel4 = false;
  bool _isSel5 = false;
  bool _isSel6 = false;
  bool _isSel7 = false;
  bool _isSel8 = false;
  bool _isSel9 = false;
  bool _isSel10 = false;
  bool _isSel11 = false;
  bool _isSel12 = false;
  bool _isSel13 = false;
  bool _isSel14 = false;
  bool _isSel15 = false;
  bool _isSel16 = false;
  bool _isSel17 = false;
  bool _isSel18 = false;
  bool _isSel19 = false;
  bool _isSel20 = false;
  bool _isSel21 = false;
  bool _isSel22 = false;
  bool _isSel23 = false;
  bool _isSel24 = false;
  bool _isSel25 = false;
  bool _isSel26 = false;
  bool _isSel27 = false;
  bool _isSel28 = false;
  bool _isSel29 = false;
  bool _isSel30 = false;
  bool _isSel31 = false;
  bool _isSel32 = false;
  bool _isSel33 = false;
  bool _isSel34 = false;
  bool _isSel35 = false;
  bool _isSel36 = false;
  bool _isSel37 = false;
  bool _isSel38 = false;
  bool _isSel39 = false;
  bool _isSel40 = false;
  bool _isSel41 = false;
  bool _isSel42 = false;
  bool _isSel43 = false;
  bool _isSel44 = false;
  bool _isSel45 = false;
  bool _isSel46 = false;
  bool _isSel47 = false;
  bool _isSel48 = false;
  bool _isSel49 = false;
  bool _isSel50 = false;
  bool _isSel51 = false;
  bool _isSel52 = false;
  bool _isSel53 = false;
  bool _isSel54 = false;
  bool _isSel55 = false;
  bool _isSel56 = false;
  bool _isSel57 = false;
  bool _isSel58 = false;
  bool _isSel59 = false;

  int _cantidadKids = 0;
  int _cantidadAdultos = 0;
  int _cantidadTerEd = 0;
  PreferenciasUsuario _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    PeliculaAsiento pelicula = ModalRoute.of(context).settings.arguments;

    if (MediaQuery.of(context).size.width < 450) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF212121),
          title: Text(''),
          centerTitle: true,
          actions: _listaActions(context),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              _posterTituloRes(context, pelicula),
              _totalPago(pelicula),
              _asientosRes(),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xFF212121),
            title: Text(pelicula.title),
            centerTitle: true,
            actions: _listaActions(context),
          ),
          body: Container(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _posterTitulo(context, pelicula),
                _asientos(),
                _totalPago(pelicula)
              ],
            ),
          ));
    }
  }

  Widget _totalPago(PeliculaAsiento pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            'Detalles',
            style: Theme.of(context).textTheme.headline3,
            textAlign: TextAlign.right,
          ),
          Text(
            'Niños \$40.0',
            textAlign: TextAlign.right,
            style: Theme.of(context).textTheme.headline5,
          ),
          _crearKids(),
          Text(
            'Adultos \$70.0',
            textAlign: TextAlign.right,
            style: Theme.of(context).textTheme.headline5,
          ),
          _crearAdultos(),
          Text(
            'Tercera Edad \$50.0',
            textAlign: TextAlign.right,
            style: Theme.of(context).textTheme.headline5,
          ),
          _crearTerEd(),
          Text(
            'Total: \$' +
                (_cantidadKids * 40.0 +
                        _cantidadAdultos * 70.0 +
                        _cantidadTerEd * 50.0)
                    .toString(),
            style: Theme.of(context).textTheme.headline4,
          ),
          RaisedButton(
            child: Text('Pagar'),
            onPressed: () {
              if ((_cantidadKids + _cantidadAdultos + _cantidadTerEd) > 0) {
                if (asientos.length > 0) {
                  if (_prefs.boletos == null) {
                    List<String> bol = new List();
                    String listAsientos = "";
                    asientos.forEach((element) {
                      listAsientos = element + " | ";
                    });
                    int cantidadBoletos =
                        _cantidadKids + _cantidadAdultos + _cantidadTerEd;
                    String boleto =
                        "${_prefs.username}-${pelicula.title}-${pelicula.getCoverImg()}-${pelicula.getPosterImg()}-Horario:${pelicula.horario}-Boletos:${cantidadBoletos.toString()}-Asientos:$listAsientos";
                    bol.add(boleto);
                    _prefs.boletos = bol;
                    Navigator.pushNamed(context, 'perfil');
                  } else {
                    List<String> bol = _prefs.boletos;
                    String listAsientos = "";
                    asientos.forEach((element) {
                      listAsientos = element + " | ";
                    });
                    int cantidadBoletos =
                        _cantidadKids + _cantidadAdultos + _cantidadTerEd;
                    String boleto =
                        "${_prefs.username}-${pelicula.title}-${pelicula.getCoverImg()}-${pelicula.getPosterImg()}-Horario:${pelicula.horario}-Boletos:${cantidadBoletos.toString()}-Asientos:$listAsientos";
                    bol.add(boleto);
                    _prefs.boletos = bol;
                    Navigator.pushNamed(context, 'perfil');
                  }
                } else {
                  _showPagar(context, 'Falta elegir tus asientos');
                }
              } else {
                _showPagar(context, 'Falta elegir el tipo de boleto');
              }
            },
          )
        ],
      ),
    );
  }

  Widget _asientos() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'Asientos',
            style: Theme.of(context).textTheme.headline3,
          ),
          Container(
            width: 500,
            child: GridView.count(
              childAspectRatio: 1,
              shrinkWrap: true,
              crossAxisCount: 10,
              children: _crearAsientos(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _asientosRes() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'Asientos',
            style: Theme.of(context).textTheme.headline3,
          ),
          Container(
            width: 280,
            child: GridView.count(
              childAspectRatio: 1,
              shrinkWrap: true,
              crossAxisCount: 10,
              children: _crearAsientosRes(),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _crearAsientos() {
    List<Widget> lista = new List();

    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel) {
              _iconColor = Colors.blue;
              _isSel = true;
              asientos.add('0');
            } else {
              _iconColor = Colors.red;
              _isSel = false;
              asientos.remove('0');
            }
          });
        },
      ),
    );

    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor1,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel1) {
              _iconColor1 = Colors.blue;
              _isSel1 = true;
              asientos.add('1');
            } else {
              _iconColor1 = Colors.red;
              _isSel1 = false;
              asientos.remove('1');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor2,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel2) {
              _iconColor2 = Colors.blue;
              _isSel2 = true;
              asientos.add('2');
            } else {
              _iconColor2 = Colors.red;
              _isSel2 = false;
              asientos.remove('2');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor3,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel3) {
              _iconColor3 = Colors.blue;
              _isSel3 = true;
              asientos.add('3');
            } else {
              _iconColor3 = Colors.red;
              _isSel3 = false;
              asientos.remove('3');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor4,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel4) {
              _iconColor4 = Colors.blue;
              _isSel4 = true;
              asientos.add('4');
            } else {
              _iconColor4 = Colors.red;
              _isSel4 = false;
              asientos.remove('4');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor5,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel5) {
              _iconColor5 = Colors.blue;
              _isSel5 = true;
              asientos.add('5');
            } else {
              _iconColor5 = Colors.red;
              _isSel5 = false;
              asientos.remove('5');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor6,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel6) {
              _iconColor6 = Colors.blue;
              _isSel6 = true;
              asientos.add('6');
            } else {
              _iconColor6 = Colors.red;
              _isSel6 = false;
              asientos.remove('6');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor7,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel7) {
              _iconColor7 = Colors.blue;
              _isSel7 = true;
              asientos.add('7');
            } else {
              _iconColor7 = Colors.red;
              _isSel7 = false;
              asientos.remove('7');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor8,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel8) {
              _iconColor8 = Colors.blue;
              _isSel8 = true;
              asientos.add('8');
            } else {
              _iconColor8 = Colors.red;
              _isSel8 = false;
              asientos.remove('8');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor9,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel9) {
              _iconColor9 = Colors.blue;
              _isSel9 = true;
              asientos.add('9');
            } else {
              _iconColor9 = Colors.red;
              _isSel9 = false;
              asientos.remove('9');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor10,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel10) {
              _iconColor10 = Colors.blue;
              _isSel10 = true;
              asientos.add('10');
            } else {
              _iconColor10 = Colors.red;
              _isSel10 = false;
              asientos.remove('10');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor11,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel11) {
              _iconColor11 = Colors.blue;
              _isSel11 = true;
              asientos.add('11');
            } else {
              _iconColor11 = Colors.red;
              _isSel11 = false;
              asientos.remove('11');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor12,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel12) {
              _iconColor12 = Colors.blue;
              _isSel12 = true;
              asientos.add('12');
            } else {
              _iconColor12 = Colors.red;
              _isSel12 = false;
              asientos.remove('12');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor13,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel13) {
              _iconColor13 = Colors.blue;
              _isSel13 = true;
              asientos.add('13');
            } else {
              _iconColor13 = Colors.red;
              _isSel13 = false;
              asientos.remove('13');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor14,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel14) {
              _iconColor14 = Colors.blue;
              _isSel14 = true;
              asientos.add('14');
            } else {
              _iconColor14 = Colors.red;
              _isSel14 = false;
              asientos.remove('14');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor15,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel15) {
              _iconColor15 = Colors.blue;
              _isSel15 = true;
              asientos.add('15');
            } else {
              _iconColor15 = Colors.red;
              _isSel15 = false;
              asientos.remove('15');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor16,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel16) {
              _iconColor16 = Colors.blue;
              _isSel16 = true;
              asientos.add('16');
            } else {
              _iconColor16 = Colors.red;
              _isSel16 = false;
              asientos.remove('16');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor17,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel17) {
              _iconColor17 = Colors.blue;
              _isSel17 = true;
              asientos.add('17');
            } else {
              _iconColor17 = Colors.red;
              _isSel17 = false;
              asientos.remove('17');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor18,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel18) {
              _iconColor18 = Colors.blue;
              _isSel18 = true;
              asientos.add('18');
            } else {
              _iconColor18 = Colors.red;
              _isSel18 = false;
              asientos.remove('18');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor19,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel19) {
              _iconColor19 = Colors.blue;
              _isSel19 = true;
              asientos.add('19');
            } else {
              _iconColor19 = Colors.red;
              _isSel19 = false;
              asientos.remove('19');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor20,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel20) {
              _iconColor20 = Colors.blue;
              _isSel20 = true;
              asientos.add('20');
            } else {
              _iconColor20 = Colors.red;
              _isSel20 = false;
              asientos.remove('20');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor21,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel21) {
              _iconColor21 = Colors.blue;
              _isSel21 = true;
              asientos.add('21');
            } else {
              _iconColor21 = Colors.red;
              _isSel21 = false;
              asientos.remove('21');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor22,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel22) {
              _iconColor22 = Colors.blue;
              _isSel22 = true;
              asientos.add('22');
            } else {
              _iconColor22 = Colors.red;
              _isSel22 = false;
              asientos.remove('22');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor23,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel23) {
              _iconColor23 = Colors.blue;
              _isSel23 = true;
              asientos.add('23');
            } else {
              _iconColor23 = Colors.red;
              _isSel23 = false;
              asientos.remove('23');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor24,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel24) {
              _iconColor24 = Colors.blue;
              _isSel24 = true;
              asientos.add('24');
            } else {
              _iconColor24 = Colors.red;
              _isSel24 = false;
              asientos.remove('24');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor25,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel25) {
              _iconColor25 = Colors.blue;
              _isSel25 = true;
              asientos.add('25');
            } else {
              _iconColor25 = Colors.red;
              _isSel25 = false;
              asientos.remove('25');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor26,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel26) {
              _iconColor26 = Colors.blue;
              _isSel26 = true;
              asientos.add('26');
            } else {
              _iconColor26 = Colors.red;
              _isSel26 = false;
              asientos.remove('26');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor27,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel27) {
              _iconColor27 = Colors.blue;
              _isSel27 = true;
              asientos.add('27');
            } else {
              _iconColor27 = Colors.red;
              _isSel27 = false;
              asientos.remove('27');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor28,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel28) {
              _iconColor28 = Colors.blue;
              _isSel28 = true;
              asientos.add('28');
            } else {
              _iconColor28 = Colors.red;
              _isSel28 = false;
              asientos.remove('28');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor29,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel29) {
              _iconColor29 = Colors.blue;
              _isSel29 = true;
              asientos.add('29');
            } else {
              _iconColor29 = Colors.red;
              _isSel29 = false;
              asientos.remove('29');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor30,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel30) {
              _iconColor30 = Colors.blue;
              _isSel30 = true;
              asientos.add('30');
            } else {
              _iconColor30 = Colors.red;
              _isSel30 = false;
              asientos.remove('30');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor31,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel31) {
              _iconColor31 = Colors.blue;
              _isSel31 = true;
              asientos.add('31');
            } else {
              _iconColor31 = Colors.red;
              _isSel31 = false;
              asientos.remove('31');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor32,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel32) {
              _iconColor32 = Colors.blue;
              _isSel32 = true;
              asientos.add('32');
            } else {
              _iconColor32 = Colors.red;
              _isSel32 = false;
              asientos.remove('32');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor33,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel33) {
              _iconColor33 = Colors.blue;
              _isSel33 = true;
              asientos.add('33');
            } else {
              _iconColor33 = Colors.red;
              _isSel33 = false;
              asientos.remove('33');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor34,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel34) {
              _iconColor34 = Colors.blue;
              _isSel34 = true;
              asientos.add('34');
            } else {
              _iconColor34 = Colors.red;
              _isSel34 = false;
              asientos.remove('34');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor35,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel35) {
              _iconColor35 = Colors.blue;
              _isSel35 = true;
              asientos.add('35');
            } else {
              _iconColor35 = Colors.red;
              _isSel35 = false;
              asientos.remove('35');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor36,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel36) {
              _iconColor36 = Colors.blue;
              _isSel36 = true;
              asientos.add('36');
            } else {
              _iconColor36 = Colors.red;
              _isSel36 = false;
              asientos.remove('36');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor37,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel37) {
              _iconColor37 = Colors.blue;
              _isSel37 = true;
              asientos.add('37');
            } else {
              _iconColor37 = Colors.red;
              _isSel37 = false;
              asientos.remove('37');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor38,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel38) {
              _iconColor38 = Colors.blue;
              _isSel38 = true;
              asientos.add('38');
            } else {
              _iconColor38 = Colors.red;
              _isSel38 = false;
              asientos.remove('38');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor39,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel39) {
              _iconColor39 = Colors.blue;
              _isSel39 = true;
              asientos.add('39');
            } else {
              _iconColor39 = Colors.red;
              _isSel39 = false;
              asientos.remove('39');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor40,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel40) {
              _iconColor40 = Colors.blue;
              _isSel40 = true;
              asientos.add('40');
            } else {
              _iconColor40 = Colors.red;
              _isSel40 = false;
              asientos.remove('40');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor41,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel41) {
              _iconColor41 = Colors.blue;
              _isSel41 = true;
              asientos.add('41');
            } else {
              _iconColor41 = Colors.red;
              _isSel41 = false;
              asientos.remove('41');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor42,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel42) {
              _iconColor42 = Colors.blue;
              _isSel42 = true;
              asientos.add('42');
            } else {
              _iconColor42 = Colors.red;
              _isSel42 = false;
              asientos.remove('42');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor43,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel43) {
              _iconColor43 = Colors.blue;
              _isSel43 = true;
              asientos.add('43');
            } else {
              _iconColor43 = Colors.red;
              _isSel43 = false;
              asientos.remove('43');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor44,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel44) {
              _iconColor44 = Colors.blue;
              _isSel44 = true;
              asientos.add('44');
            } else {
              _iconColor44 = Colors.red;
              _isSel44 = false;
              asientos.remove('44');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor45,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel45) {
              _iconColor45 = Colors.blue;
              _isSel45 = true;
              asientos.add('45');
            } else {
              _iconColor45 = Colors.red;
              _isSel45 = false;
              asientos.remove('45');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor46,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel46) {
              _iconColor46 = Colors.blue;
              _isSel46 = true;
              asientos.add('46');
            } else {
              _iconColor46 = Colors.red;
              _isSel46 = false;
              asientos.remove('46');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor47,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel47) {
              _iconColor47 = Colors.blue;
              _isSel47 = true;
              asientos.add('47');
            } else {
              _iconColor47 = Colors.red;
              _isSel47 = false;
              asientos.remove('47');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor48,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel48) {
              _iconColor48 = Colors.blue;
              _isSel48 = true;
              asientos.add('48');
            } else {
              _iconColor48 = Colors.red;
              _isSel48 = false;
              asientos.remove('48');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor49,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel49) {
              _iconColor49 = Colors.blue;
              _isSel49 = true;
              asientos.add('49');
            } else {
              _iconColor49 = Colors.red;
              _isSel49 = false;
              asientos.remove('49');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor50,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel50) {
              _iconColor50 = Colors.blue;
              _isSel50 = true;
              asientos.add('50');
            } else {
              _iconColor50 = Colors.red;
              _isSel50 = false;
              asientos.remove('50');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor51,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel51) {
              _iconColor51 = Colors.blue;
              _isSel51 = true;
              asientos.add('51');
            } else {
              _iconColor51 = Colors.red;
              _isSel51 = false;
              asientos.remove('51');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor52,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel52) {
              _iconColor52 = Colors.blue;
              _isSel52 = true;
              asientos.add('52');
            } else {
              _iconColor52 = Colors.red;
              _isSel52 = false;
              asientos.remove('52');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor53,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel53) {
              _iconColor53 = Colors.blue;
              _isSel53 = true;
              asientos.add('53');
            } else {
              _iconColor53 = Colors.red;
              _isSel53 = false;
              asientos.remove('53');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor54,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel54) {
              _iconColor54 = Colors.blue;
              _isSel54 = true;
              asientos.add('54');
            } else {
              _iconColor54 = Colors.red;
              _isSel54 = false;
              asientos.remove('54');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor55,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel55) {
              _iconColor55 = Colors.blue;
              _isSel55 = true;
              asientos.add('55');
            } else {
              _iconColor55 = Colors.red;
              _isSel55 = false;
              asientos.remove('55');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor56,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel56) {
              _iconColor56 = Colors.blue;
              _isSel56 = true;
              asientos.add('56');
            } else {
              _iconColor56 = Colors.red;
              _isSel56 = false;
              asientos.remove('56');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor57,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel57) {
              _iconColor57 = Colors.blue;
              _isSel57 = true;
              asientos.add('57');
            } else {
              _iconColor57 = Colors.red;
              _isSel57 = false;
              asientos.remove('57');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor58,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel58) {
              _iconColor58 = Colors.blue;
              _isSel58 = true;
              asientos.add('58');
            } else {
              _iconColor58 = Colors.red;
              _isSel58 = false;
              asientos.remove('58');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 48,
          color: _iconColor59,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel59) {
              _iconColor59 = Colors.blue;
              _isSel59 = true;
              asientos.add('59');
            } else {
              _iconColor59 = Colors.red;
              _isSel59 = false;
              asientos.remove('59');
            }
          });
        },
      ),
    );

    return lista;
  }

  List<Widget> _crearAsientosRes() {
    List<Widget> lista = new List();

    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel) {
              _iconColor = Colors.blue;
              _isSel = true;
              asientos.add('0');
            } else {
              _iconColor = Colors.red;
              _isSel = false;
              asientos.remove('0');
            }
          });
        },
      ),
    );

    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor1,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel1) {
              _iconColor1 = Colors.blue;
              _isSel1 = true;
              asientos.add('1');
            } else {
              _iconColor1 = Colors.red;
              _isSel1 = false;
              asientos.remove('1');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor2,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel2) {
              _iconColor2 = Colors.blue;
              _isSel2 = true;
              asientos.add('2');
            } else {
              _iconColor2 = Colors.red;
              _isSel2 = false;
              asientos.remove('2');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor3,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel3) {
              _iconColor3 = Colors.blue;
              _isSel3 = true;
              asientos.add('3');
            } else {
              _iconColor3 = Colors.red;
              _isSel3 = false;
              asientos.remove('3');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor4,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel4) {
              _iconColor4 = Colors.blue;
              _isSel4 = true;
              asientos.add('4');
            } else {
              _iconColor4 = Colors.red;
              _isSel4 = false;
              asientos.remove('4');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor5,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel5) {
              _iconColor5 = Colors.blue;
              _isSel5 = true;
              asientos.add('5');
            } else {
              _iconColor5 = Colors.red;
              _isSel5 = false;
              asientos.remove('5');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor6,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel6) {
              _iconColor6 = Colors.blue;
              _isSel6 = true;
              asientos.add('6');
            } else {
              _iconColor6 = Colors.red;
              _isSel6 = false;
              asientos.remove('6');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor7,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel7) {
              _iconColor7 = Colors.blue;
              _isSel7 = true;
              asientos.add('7');
            } else {
              _iconColor7 = Colors.red;
              _isSel7 = false;
              asientos.remove('7');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor8,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel8) {
              _iconColor8 = Colors.blue;
              _isSel8 = true;
              asientos.add('8');
            } else {
              _iconColor8 = Colors.red;
              _isSel8 = false;
              asientos.remove('8');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor9,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel9) {
              _iconColor9 = Colors.blue;
              _isSel9 = true;
              asientos.add('9');
            } else {
              _iconColor9 = Colors.red;
              _isSel9 = false;
              asientos.remove('9');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor10,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel10) {
              _iconColor10 = Colors.blue;
              _isSel10 = true;
              asientos.add('10');
            } else {
              _iconColor10 = Colors.red;
              _isSel10 = false;
              asientos.remove('10');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor11,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel11) {
              _iconColor11 = Colors.blue;
              _isSel11 = true;
              asientos.add('11');
            } else {
              _iconColor11 = Colors.red;
              _isSel11 = false;
              asientos.remove('11');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor12,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel12) {
              _iconColor12 = Colors.blue;
              _isSel12 = true;
              asientos.add('12');
            } else {
              _iconColor12 = Colors.red;
              _isSel12 = false;
              asientos.remove('12');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor13,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel13) {
              _iconColor13 = Colors.blue;
              _isSel13 = true;
              asientos.add('13');
            } else {
              _iconColor13 = Colors.red;
              _isSel13 = false;
              asientos.remove('13');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor14,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel14) {
              _iconColor14 = Colors.blue;
              _isSel14 = true;
              asientos.add('14');
            } else {
              _iconColor14 = Colors.red;
              _isSel14 = false;
              asientos.remove('14');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor15,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel15) {
              _iconColor15 = Colors.blue;
              _isSel15 = true;
              asientos.add('15');
            } else {
              _iconColor15 = Colors.red;
              _isSel15 = false;
              asientos.remove('15');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor16,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel16) {
              _iconColor16 = Colors.blue;
              _isSel16 = true;
              asientos.add('16');
            } else {
              _iconColor16 = Colors.red;
              _isSel16 = false;
              asientos.remove('16');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor17,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel17) {
              _iconColor17 = Colors.blue;
              _isSel17 = true;
              asientos.add('17');
            } else {
              _iconColor17 = Colors.red;
              _isSel17 = false;
              asientos.remove('17');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor18,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel18) {
              _iconColor18 = Colors.blue;
              _isSel18 = true;
              asientos.add('18');
            } else {
              _iconColor18 = Colors.red;
              _isSel18 = false;
              asientos.remove('18');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor19,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel19) {
              _iconColor19 = Colors.blue;
              _isSel19 = true;
              asientos.add('19');
            } else {
              _iconColor19 = Colors.red;
              _isSel19 = false;
              asientos.remove('19');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor20,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel20) {
              _iconColor20 = Colors.blue;
              _isSel20 = true;
              asientos.add('20');
            } else {
              _iconColor20 = Colors.red;
              _isSel20 = false;
              asientos.remove('20');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor21,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel21) {
              _iconColor21 = Colors.blue;
              _isSel21 = true;
              asientos.add('21');
            } else {
              _iconColor21 = Colors.red;
              _isSel21 = false;
              asientos.remove('21');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor22,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel22) {
              _iconColor22 = Colors.blue;
              _isSel22 = true;
              asientos.add('22');
            } else {
              _iconColor22 = Colors.red;
              _isSel22 = false;
              asientos.remove('22');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor23,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel23) {
              _iconColor23 = Colors.blue;
              _isSel23 = true;
              asientos.add('23');
            } else {
              _iconColor23 = Colors.red;
              _isSel23 = false;
              asientos.remove('23');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor24,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel24) {
              _iconColor24 = Colors.blue;
              _isSel24 = true;
              asientos.add('24');
            } else {
              _iconColor24 = Colors.red;
              _isSel24 = false;
              asientos.remove('24');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor25,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel25) {
              _iconColor25 = Colors.blue;
              _isSel25 = true;
              asientos.add('25');
            } else {
              _iconColor25 = Colors.red;
              _isSel25 = false;
              asientos.remove('25');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor26,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel26) {
              _iconColor26 = Colors.blue;
              _isSel26 = true;
              asientos.add('26');
            } else {
              _iconColor26 = Colors.red;
              _isSel26 = false;
              asientos.remove('26');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor27,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel27) {
              _iconColor27 = Colors.blue;
              _isSel27 = true;
              asientos.add('27');
            } else {
              _iconColor27 = Colors.red;
              _isSel27 = false;
              asientos.remove('27');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor28,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel28) {
              _iconColor28 = Colors.blue;
              _isSel28 = true;
              asientos.add('28');
            } else {
              _iconColor28 = Colors.red;
              _isSel28 = false;
              asientos.remove('28');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor29,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel29) {
              _iconColor29 = Colors.blue;
              _isSel29 = true;
              asientos.add('29');
            } else {
              _iconColor29 = Colors.red;
              _isSel29 = false;
              asientos.remove('29');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor30,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel30) {
              _iconColor30 = Colors.blue;
              _isSel30 = true;
              asientos.add('30');
            } else {
              _iconColor30 = Colors.red;
              _isSel30 = false;
              asientos.remove('30');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor31,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel31) {
              _iconColor31 = Colors.blue;
              _isSel31 = true;
              asientos.add('31');
            } else {
              _iconColor31 = Colors.red;
              _isSel31 = false;
              asientos.remove('31');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor32,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel32) {
              _iconColor32 = Colors.blue;
              _isSel32 = true;
              asientos.add('32');
            } else {
              _iconColor32 = Colors.red;
              _isSel32 = false;
              asientos.remove('32');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor33,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel33) {
              _iconColor33 = Colors.blue;
              _isSel33 = true;
              asientos.add('33');
            } else {
              _iconColor33 = Colors.red;
              _isSel33 = false;
              asientos.remove('33');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor34,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel34) {
              _iconColor34 = Colors.blue;
              _isSel34 = true;
              asientos.add('34');
            } else {
              _iconColor34 = Colors.red;
              _isSel34 = false;
              asientos.remove('34');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor35,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel35) {
              _iconColor35 = Colors.blue;
              _isSel35 = true;
              asientos.add('35');
            } else {
              _iconColor35 = Colors.red;
              _isSel35 = false;
              asientos.remove('35');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor36,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel36) {
              _iconColor36 = Colors.blue;
              _isSel36 = true;
              asientos.add('36');
            } else {
              _iconColor36 = Colors.red;
              _isSel36 = false;
              asientos.remove('36');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor37,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel37) {
              _iconColor37 = Colors.blue;
              _isSel37 = true;
              asientos.add('37');
            } else {
              _iconColor37 = Colors.red;
              _isSel37 = false;
              asientos.remove('37');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor38,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel38) {
              _iconColor38 = Colors.blue;
              _isSel38 = true;
              asientos.add('38');
            } else {
              _iconColor38 = Colors.red;
              _isSel38 = false;
              asientos.remove('38');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor39,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel39) {
              _iconColor39 = Colors.blue;
              _isSel39 = true;
              asientos.add('39');
            } else {
              _iconColor39 = Colors.red;
              _isSel39 = false;
              asientos.remove('39');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor40,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel40) {
              _iconColor40 = Colors.blue;
              _isSel40 = true;
              asientos.add('40');
            } else {
              _iconColor40 = Colors.red;
              _isSel40 = false;
              asientos.remove('40');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor41,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel41) {
              _iconColor41 = Colors.blue;
              _isSel41 = true;
              asientos.add('41');
            } else {
              _iconColor41 = Colors.red;
              _isSel41 = false;
              asientos.remove('41');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor42,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel42) {
              _iconColor42 = Colors.blue;
              _isSel42 = true;
              asientos.add('42');
            } else {
              _iconColor42 = Colors.red;
              _isSel42 = false;
              asientos.remove('42');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor43,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel43) {
              _iconColor43 = Colors.blue;
              _isSel43 = true;
              asientos.add('43');
            } else {
              _iconColor43 = Colors.red;
              _isSel43 = false;
              asientos.remove('43');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor44,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel44) {
              _iconColor44 = Colors.blue;
              _isSel44 = true;
              asientos.add('44');
            } else {
              _iconColor44 = Colors.red;
              _isSel44 = false;
              asientos.remove('44');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor45,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel45) {
              _iconColor45 = Colors.blue;
              _isSel45 = true;
              asientos.add('45');
            } else {
              _iconColor45 = Colors.red;
              _isSel45 = false;
              asientos.remove('45');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor46,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel46) {
              _iconColor46 = Colors.blue;
              _isSel46 = true;
              asientos.add('46');
            } else {
              _iconColor46 = Colors.red;
              _isSel46 = false;
              asientos.remove('46');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor47,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel47) {
              _iconColor47 = Colors.blue;
              _isSel47 = true;
              asientos.add('47');
            } else {
              _iconColor47 = Colors.red;
              _isSel47 = false;
              asientos.remove('47');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor48,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel48) {
              _iconColor48 = Colors.blue;
              _isSel48 = true;
              asientos.add('48');
            } else {
              _iconColor48 = Colors.red;
              _isSel48 = false;
              asientos.remove('48');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor49,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel49) {
              _iconColor49 = Colors.blue;
              _isSel49 = true;
              asientos.add('49');
            } else {
              _iconColor49 = Colors.red;
              _isSel49 = false;
              asientos.remove('49');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor50,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel50) {
              _iconColor50 = Colors.blue;
              _isSel50 = true;
              asientos.add('50');
            } else {
              _iconColor50 = Colors.red;
              _isSel50 = false;
              asientos.remove('50');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor51,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel51) {
              _iconColor51 = Colors.blue;
              _isSel51 = true;
              asientos.add('51');
            } else {
              _iconColor51 = Colors.red;
              _isSel51 = false;
              asientos.remove('51');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor52,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel52) {
              _iconColor52 = Colors.blue;
              _isSel52 = true;
              asientos.add('52');
            } else {
              _iconColor52 = Colors.red;
              _isSel52 = false;
              asientos.remove('52');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor53,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel53) {
              _iconColor53 = Colors.blue;
              _isSel53 = true;
              asientos.add('53');
            } else {
              _iconColor53 = Colors.red;
              _isSel53 = false;
              asientos.remove('53');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor54,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel54) {
              _iconColor54 = Colors.blue;
              _isSel54 = true;
              asientos.add('54');
            } else {
              _iconColor54 = Colors.red;
              _isSel54 = false;
              asientos.remove('54');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor55,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel55) {
              _iconColor55 = Colors.blue;
              _isSel55 = true;
              asientos.add('55');
            } else {
              _iconColor55 = Colors.red;
              _isSel55 = false;
              asientos.remove('55');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor56,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel56) {
              _iconColor56 = Colors.blue;
              _isSel56 = true;
              asientos.add('56');
            } else {
              _iconColor56 = Colors.red;
              _isSel56 = false;
              asientos.remove('56');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor57,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel57) {
              _iconColor57 = Colors.blue;
              _isSel57 = true;
              asientos.add('57');
            } else {
              _iconColor57 = Colors.red;
              _isSel57 = false;
              asientos.remove('57');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor58,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel58) {
              _iconColor58 = Colors.blue;
              _isSel58 = true;
              asientos.add('58');
            } else {
              _iconColor58 = Colors.red;
              _isSel58 = false;
              asientos.remove('58');
            }
          });
        },
      ),
    );
    lista.add(
      InkWell(
        child: Icon(
          CinemaIcons.cinema_chair,
          size: 16,
          color: _iconColor59,
        ),
        onTap: () {
          setState(() {
            if (asientos.length <
                    (_cantidadKids + _cantidadAdultos + _cantidadTerEd) &&
                !_isSel59) {
              _iconColor59 = Colors.blue;
              _isSel59 = true;
              asientos.add('59');
            } else {
              _iconColor59 = Colors.red;
              _isSel59 = false;
              asientos.remove('59');
            }
          });
        },
      ),
    );

    return lista;
  }

  Widget _posterTitulo(BuildContext context, PeliculaAsiento pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      width: 500,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Image(
                image: NetworkImage(pelicula.getCoverImg()),
                height: 600.0,
              )),
          _crearInfoPeli(pelicula),
        ],
      ),
    );
  }

  Widget _posterTituloRes(BuildContext context, PeliculaAsiento pelicula) {
    return Container(
      width: 280,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              child: Image(
            image: NetworkImage(pelicula.getCoverImg()),
            height: 430.0,
          )),
          _crearInfoPeliRes(pelicula),
        ],
      ),
    );
  }

  Widget _crearKids() {
    return Container(
      width: 500,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadKids <= 0) {
                    _cantidadKids = 0;
                  } else {
                    _cantidadKids--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadKids.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadKids++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearKidsRes() {
    return Container(
      width: 280,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadKids <= 0) {
                    _cantidadKids = 0;
                  } else {
                    _cantidadKids--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadKids.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadKids++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearAdultos() {
    return Container(
      width: 500,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadAdultos <= 0) {
                    _cantidadAdultos = 0;
                  } else {
                    _cantidadAdultos--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadAdultos.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadAdultos++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearAdultosRes() {
    return Container(
      width: 280,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadAdultos <= 0) {
                    _cantidadAdultos = 0;
                  } else {
                    _cantidadAdultos--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadAdultos.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadAdultos++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearTerEd() {
    return Container(
      width: 500,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadTerEd <= 0) {
                    _cantidadTerEd = 0;
                  } else {
                    _cantidadTerEd--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadTerEd.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadTerEd++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearTerEdRes() {
    return Container(
      width: 280,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  if (_cantidadTerEd <= 0) {
                    _cantidadTerEd = 0;
                  } else {
                    _cantidadTerEd--;
                  }
                });
              }),
          Container(
            color: Colors.white,
            child: Text(_cantidadTerEd.toString()),
          ),
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  _cantidadTerEd++;
                });
              }),
        ],
      ),
    );
  }

  Widget _crearInfoPeli(PeliculaAsiento pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text(
              '130 min',
              style: TextStyle(color: Colors.white),
            ),
            color: Color(0xFF212121),
            onPressed: () {},
          ),
          RaisedButton(
            child: Text(
              'B-15',
              style: TextStyle(color: Colors.white),
            ),
            color: Color(0xFF43A047),
            onPressed: () {},
          ),
          RaisedButton(
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.star,
                  color: Colors.white,
                ),
                Text(
                  pelicula.voteAverage.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
            color: Color(0xFFfbc02d),
            onPressed: () {},
          ),
          RaisedButton(
            child: Text(
              pelicula.horario,
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blue,
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _crearInfoPeliRes(PeliculaAsiento pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text(
              '130 min',
              style: TextStyle(color: Colors.white),
            ),
            color: Color(0xFF212121),
            onPressed: () {},
          ),
          RaisedButton(
            child: Text(
              pelicula.horario,
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blue,
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  List<Widget> _listaActions(BuildContext context) {
    List<Widget> lista = new List();

    if (!_prefs.isLoggedIn) {
      lista.add(FlatButton(
        onPressed: () {
          _showDialog(context);
        },
        child: Text(
          'Iniciar Sesión',
          style: TextStyle(color: Colors.white),
        ),
      ));
      lista.add(FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, 'registrar');
        },
        child: Text(
          'Registrarse',
          style: TextStyle(color: Colors.white),
        ),
      ));
    } else {
      final usuario = IconButton(
          icon: Icon(Icons.person),
          onPressed: () {
            _showPerfil(context);
          });
      lista.add(usuario);
    }

    return lista;
  }

  void _showDialog(context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          title: new Text('Iniciar Sesión'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Ingresa tus credenciales para iniciar sesión'),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Usuario'),
                  onChanged: (value) {
                    _usuario = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password'),
                  onChanged: (value) {
                    _password = value;
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Salir',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Iniciar Sesión',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {},
            ),
          ],
        );
      },
    );
  }

  void _showPagar(context, String msg) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          //title: new Text('Iniciar Sesión'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '¡Ups!',
                  style: Theme.of(context).textTheme.headline3,
                ),
                Text(msg),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Entendido',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showPerfil(context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          //title: new Text('Iniciar Sesión'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '¡Ups!',
                  style: Theme.of(context).textTheme.headline3,
                ),
                Text(
                    '¿Está seguro de que quiere salir? Los datos de venta no se guardarán'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Regresar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Ir a Perfil',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'perfil');
              },
            ),
          ],
        );
      },
    );
  }
}
