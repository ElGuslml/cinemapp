import 'package:cinemapp2/src/models/usuario_model.dart';
import 'package:cinemapp2/src/providers/db_provider.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:flutter/material.dart';

class RegistrarPage extends StatefulWidget {
  RegistrarPage({Key key}) : super(key: key);

  @override
  _RegistrarPageState createState() => _RegistrarPageState();
}

class _RegistrarPageState extends State<RegistrarPage> {
  String _usuario = "";
  String _password = "";
  String _confPass = "";
  String _nombre = "";
  String _apellido = "";
  String _correo = "";

  PreferenciasUsuario _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 450) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('CinemApp'),
        ),
        body: Container(
          child: ListView(
            children: <Widget>[_ladoDerecho()],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Container(
          color: Colors.white10,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _ladoIzquierdo(context),
              _ladoDerecho(),
            ],
          ),
        ),
      );
    }
  }

  Widget _ladoIzquierdo(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width / 2,
      child: Container(
        color: Color(0xFF212121),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'CinemApp',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 80.0,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              'Tu mejor opción para disfrutar tu pelicula',
              style: TextStyle(color: Colors.white, fontSize: 30.0),
            ),
            Icon(
              Icons.local_movies,
              size: 80.0,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  Widget _ladoDerecho() {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width / 2,
      child: Container(
        width: 500,
        child: Card(
          elevation: 5.0,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '¡Bienvenido!',
                  style: Theme.of(context).textTheme.headline3,
                ),
                Text('Ingresa los siguientes datos para poder registrarte'),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Username'),
                  onChanged: (value) {
                    _usuario = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Nombre'),
                  onChanged: (value) {
                    _nombre = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Apellido'),
                  onChanged: (value) {
                    _apellido = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Contraseña'),
                  onChanged: (value) {
                    _password = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Confirmar contraseña'),
                  onChanged: (value) {
                    _confPass = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: RaisedButton(
                        color: Color(0xFF212121),
                        child: Text(
                          'Registrarse',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_nombre != "" &&
                              _apellido != "" &&
                              _password != "" &&
                              _usuario != "") {
                            if (_password == _confPass) {
                              UsuarioModel user = new UsuarioModel();
                              user.nombre = _nombre;
                              user.apellido = _apellido;
                              user.correo = _correo;
                              user.password = _password;
                              user.username = _usuario;
                              String perfil =
                                  "$_nombre-$_apellido-$_password-$_usuario";
                              if (_prefs.perfiles == null) {
                                print('NULL');
                                List<String> perfiles = new List();
                                perfiles.add(perfil);
                                _prefs.perfiles = perfiles;
                                Navigator.pushNamed(context, 'home');
                              } else {
                                print('YA HAY REGISTROS');
                                List<String> perfiles = _prefs.perfiles;
                                perfiles.add(perfil);
                                _prefs.perfiles = perfiles;
                                Navigator.pushNamed(context, 'home');
                              }
                            } else {
                              _showDialog(
                                  context, "Las contraseñas no son iguales");
                            }
                          } else {
                            _showDialog(context, "Faltaron campos por llenar");
                          }
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showDialog(context, String msg) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          //title: new Text('Iniciar Sesión'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '¡Ups!',
                  style: Theme.of(context).textTheme.headline3,
                ),
                Text(msg),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Entendido',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
