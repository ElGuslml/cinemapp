import 'package:cinemapp2/src/providers/db_provider.dart';
import 'package:cinemapp2/src/providers/shared_provider.dart';
import 'package:flutter/material.dart';

import 'package:cinemapp2/src/models/pelicula_model.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:cinemapp2/src/providers/peliculas_provider.dart';
import 'package:cinemapp2/src/widgets/card_swiper_widget.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatefulWidget {
  HomePage({Key, key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final peliculasProvider = new PeliculasProvider();
  final _prefs = new PreferenciasUsuario();
  String _usuario = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 450) {
      print(MediaQuery.of(context).size.width);
      return Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            _crearAppBarResponsiva(context),
            SliverList(
                delegate: SliverChildListDelegate([
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Cartelera de hoy',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              _carteleraHoyRes(context),
            ]))
          ],
        ),
      );
    } else {
      return Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            _crearAppBar(context),
            SliverList(
                delegate: SliverChildListDelegate([
              Padding(padding: EdgeInsets.all(48)),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 36),
                child: Text(
                  'Cartelera de hoy',
                  style: TextStyle(fontSize: 36),
                  textAlign: TextAlign.left,
                ),
              ),
              _carteleraHoy(context),
            ]))
          ],
        ),
      );
    }
  }

  Widget _crearAppBar(BuildContext context) {
    return SliverAppBar(
      leading: IconButton(
        icon: Icon(Icons.home),
        onPressed: () {
          Navigator.pushNamed(context, 'home');
        },
      ),
      backgroundColor: Color(0xFF212121),
      expandedHeight: 500,
      floating: false,
      pinned: true,
      actions: _listaActions(context),
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Text('CinemApp'),
          background: _swiperTarjetas()),
    );
  }

  Widget _crearAppBarResponsiva(BuildContext context) {
    return SliverAppBar(
      leading: IconButton(
        icon: Icon(Icons.home),
        onPressed: () {
          Navigator.pushNamed(context, 'home');
        },
      ),
      backgroundColor: Color(0xFF212121),
      expandedHeight: 500,
      floating: false,
      pinned: true,
      actions: _listaActions(context),
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Text(''),
          background: _swiperTarjetasResp()),
    );
  }

  Widget _carteleraHoyRes(BuildContext cont) {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _gridCarteleraRes(snapshot.data, context);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  List<Widget> _listaActions(BuildContext context) {
    List<Widget> lista = new List();

    if (!_prefs.isLoggedIn) {
      lista.add(FlatButton(
        onPressed: () {
          _showDialog(context);
        },
        child: Text(
          'Iniciar Sesión',
          style: TextStyle(color: Colors.white),
        ),
      ));
      lista.add(FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, 'registrar');
        },
        child: Text(
          'Registrarse',
          style: TextStyle(color: Colors.white),
        ),
      ));
    } else {
      final usuario = IconButton(
          icon: Icon(Icons.person),
          onPressed: () {
            Navigator.pushNamed(context, 'perfil');
          });
      lista.add(usuario);
    }

    return lista;
  }

  Widget _tarjetaPeliculas(Pelicula pelicula, BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, 'detalle', arguments: pelicula);
        },
        child: FadeInImage(
            fit: BoxFit.fill,
            placeholder: AssetImage('assets/img/no-image.jpg'),
            image: NetworkImage(pelicula.getCoverImg())));
  }

  Widget _gridCartelera(List<Pelicula> peliculas, BuildContext context) {
    return Container(
      padding: EdgeInsets.all(36),
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 4,
        childAspectRatio: 0.7,
        mainAxisSpacing: 20,
        crossAxisSpacing: 20,
        physics: ScrollPhysics(),
        children: List.generate(peliculas.length, (index) {
          return _tarjetaPeliculas(peliculas[index], context);
        }),
      ),
    );
  }

  Widget _gridCarteleraRes(List<Pelicula> peliculas, BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        childAspectRatio: 0.7,
        mainAxisSpacing: 20,
        crossAxisSpacing: 20,
        physics: ScrollPhysics(),
        children: List.generate(peliculas.length, (index) {
          return _tarjetaPeliculas(peliculas[index], context);
        }),
      ),
    );
  }

  Widget _carteleraHoy(BuildContext cont) {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _gridCartelera(snapshot.data, context);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _swiperTarjetasResp() {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _swiperTR(snapshot.data);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _swiperTR(List<Pelicula> peliculas) {
    return Container(
      width: double.infinity,
      height: 500.0,
      child: Swiper(
        itemWidth: double.infinity,
        itemHeight: 500.0,
        itemBuilder: (BuildContext context, int index) {
          return Stack(
            fit: StackFit.expand,
            children: <Widget>[
              FadeInImage(
                alignment: Alignment.topCenter,
                fit: BoxFit.cover,
                image: NetworkImage(peliculas[index].getCoverImg()),
                placeholder: AssetImage('assets/img/no-image.jpg'),
              ),
              Container(
                color: Color.fromRGBO(0, 0, 0, 0.3),
              ),
              Positioned(
                height: 300,
                bottom: 20,
                left: 20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 14),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.star,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text(
                                    peliculas[index].voteAverage.toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          );
        },
        itemCount: peliculas.length,
        autoplayDelay: 6000,
        autoplay: true,
      ),
    );
  }

  Widget _swiperTarjetas() {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return CardSwiper(peliculas: snapshot.data);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  void _showDialog(context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          title: new Text('Iniciar Sesión'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Ingresa tus credenciales para iniciar sesión'),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Usuario'),
                  onChanged: (value) {
                    _usuario = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password'),
                  onChanged: (value) {
                    _password = value;
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Salir',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            RaisedButton(
              color: Color(0xFF212121),
              child: Text(
                'Iniciar Sesión',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                SharedProvider shr = new SharedProvider();
                final res = shr.getLogin(_usuario, _password);
                if (res) {
                  Navigator.pop(context);
                }
                setState(() {});
              },
            ),
          ],
        );
      },
    );
  }
}
