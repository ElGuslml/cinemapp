import 'package:cinemapp2/src/pages/asientos_page.dart';
import 'package:cinemapp2/src/pages/home_page.dart';
import 'package:cinemapp2/src/pages/pelicula_detalle.dart';
import 'package:cinemapp2/src/pages/perfil_page.dart';
import 'package:cinemapp2/src/pages/registrar_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'home': (BuildContext context) => HomePage(),
    'detalle': (BuildContext context) => PeliculaDetalle(),
    'registrar': (BuildContext context) => RegistrarPage(),
    'asientos': (BuildContext context) => AsientosPage(),
    'perfil': (BuildContext context) => PerfilPage()
  };
}
