import 'package:cinemapp2/src/models/pelicula_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CardSwiper extends StatelessWidget {
  final List<Pelicula> peliculas;

  CardSwiper({@required this.peliculas});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 500.0,
      child: Swiper(
        itemWidth: double.infinity,
        itemHeight: 500.0,
        itemBuilder: (BuildContext context, int index) {
          /*return Container(
            child: new FadeInImage(
              alignment: Alignment.topCenter,
              fit: BoxFit.fitWidth,
              image: NetworkImage(peliculas[index].getPosterImg(),),
              placeholder: AssetImage('assets/img/no-image.jpg'),
            ),
          );*/
          return Stack(
            fit: StackFit.expand,
            children: <Widget>[
              FadeInImage(
                alignment: Alignment.topCenter,
                fit: BoxFit.fitWidth,
                image: NetworkImage(peliculas[index].getPosterImg()),
                placeholder: AssetImage('assets/img/no-image.jpg'),
              ),
              Container(
                color: Color.fromRGBO(0, 0, 0, 0.6),
              ),
              Positioned(
                height: 300,
                bottom: 20,
                left: 20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: FadeInImage(
                        image: NetworkImage(peliculas[index].getCoverImg()),
                        placeholder: AssetImage('assets/img/no-image.jpg'),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 14),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              peliculas[index].title,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 48),
                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.star,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text(
                                    peliculas[index].voteAverage.toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          );
        },
        itemCount: peliculas.length,
        autoplayDelay: 6000,
        autoplay: true,
      ),
    );
  }
}
