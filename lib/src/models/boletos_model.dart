class BoletosModel {
  String username;
  String title;
  String coverURL;
  String posterURL;
  String horario;
  String boletos;
  String asientos;

  BoletosModel(
      {this.username,
      this.title,
      this.coverURL,
      this.posterURL,
      this.horario,
      this.boletos,
      this.asientos});
}
