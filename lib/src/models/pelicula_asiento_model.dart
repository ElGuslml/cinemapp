class PeliculaAsiento {
  double popularity;
  int voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String title;
  double voteAverage;
  String overview;
  String releaseDate;
  String horario;

  PeliculaAsiento(
      {this.popularity,
      this.voteCount,
      this.video,
      this.posterPath,
      this.id,
      this.adult,
      this.backdropPath,
      this.originalLanguage,
      this.originalTitle,
      this.genreIds,
      this.title,
      this.voteAverage,
      this.overview,
      this.releaseDate,
      this.horario});

  getCoverImg() {
    if (posterPath == null) {
      return 'https://www.byustore.com/c.4325477/byu-vinson/img/no_image_available.jpeg?resizeid=2&resizeh=1200&resizew=1200';
    } else {
      return 'https://image.tmdb.org/t/p/original$posterPath';
    }
  }

  getPosterImg() {
    if (posterPath == null) {
      return 'https://www.byustore.com/c.4325477/byu-vinson/img/no_image_available.jpeg?resizeid=2&resizeh=1200&resizew=1200';
    } else {
      return 'https://image.tmdb.org/t/p/original$backdropPath';
    }
  }
}
