import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  clearPrefs() {
    _prefs.clear();
  }

  get username {
    return _prefs.getString('username') ?? "";
  }

  set username(String value) {
    _prefs.setString('username', value);
  }

  get nombre {
    return _prefs.getString('nombre') ?? "";
  }

  set nombre(String value) {
    _prefs.setString('nombre', value);
  }

  get correo {
    return _prefs.getString('correo') ?? "";
  }

  set correo(String value) {
    _prefs.setString('correo', value);
  }

  get perfiles {
    return _prefs.getStringList('perfiles') ?? null;
  }

  set perfiles(List<String> value) {
    _prefs.setStringList('perfiles', value);
  }

  get boletos {
    return _prefs.getStringList('boletos') ?? null;
  }

  set boletos(List<String> value) {
    _prefs.setStringList('boletos', value);
  }

  get isLoggedIn {
    return _prefs.getBool('isLoggedIn') ?? false;
  }

  set isLoggedIn(bool value) {
    _prefs.setBool('isLoggedIn', value);
  }
}
