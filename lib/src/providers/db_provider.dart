import 'dart:io';
import 'package:cinemapp2/src/models/usuario_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getExternalStorageDirectory();
    print('initDB');

    final path = join(documentsDirectory.path, 'ScansDB.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE usuarios ('
          ' id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,'
          ' username TEXT,'
          ' nombre TEXT,'
          ' apellido TEXT,'
          ' password TEXT,'
          ' correo TEXT');

      await db.execute(' CREATE TABLE boletos ('
          ' id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,'
          ' id_usuario INTEGER NOT NULL,'
          ' id_pelicula INTEGER,'
          ' title TEXT,'
          ' hora TEXT,'
          ' posterURL TEXT,'
          ' coverURL TEXT');
    });
  }

  nuevoUsuarioRaw(UsuarioModel user) async {
    final db = await database;

    final res = await db.rawInsert(
        "INSERT INTO usuarios(username, nombre, apellido, password) "
        "VALUES ('${user.username}', '${user.nombre}', '${user.apellido}', '${user.password}')");

    return res;
  }

  getLogin(String user, String pass) async {
    final db = await database;

    print('LOGIN');
    final res = await db.query('usuarios',
        where: 'username = ? AND password = ?', whereArgs: [user, pass]);

    return res.isNotEmpty ? res.first : null;
  }
}
