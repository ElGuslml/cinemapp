import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:cinemapp2/src/models/videos_model.dart';

class VideosProvider {
  String _apiKey = '015e346a14c96b8778e68aff714bcdcf';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  Future<List<Video>> getVideos(int movie_id) async {
    final url = Uri.https(_url, '/3/movie/$movie_id/videos',
        {'api_key': _apiKey, 'language': _language});

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    print(resp.body);

    final videos = new Videos.fromJsonList(decodedData['results']);
    return videos.items;
  }
}
