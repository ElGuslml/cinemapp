import 'package:cinemapp2/src/models/boletos_model.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:flutter/material.dart';

class SharedProvider {
  PreferenciasUsuario _prefs = new PreferenciasUsuario();

  bool getLogin(String usr, String pass) {
    List<String> perfiles = _prefs.perfiles;
    bool res = false;
    perfiles.forEach((item) {
      final perfil = item.split("-");
      if (perfil[3] == usr && perfil[2] == pass) {
        _prefs.nombre = perfil[0];
        _prefs.username = perfil[3];
        res = true;
      }
    });

    if (res) {
      _prefs.isLoggedIn = true;
    }

    return res;
  }

  List<BoletosModel> setBoletos() {
    List<String> boletos = _prefs.boletos;
    BoletosModel boletosModel = new BoletosModel();
    List<BoletosModel> res = new List();

    if (boletos != null) {
      boletos.forEach((element) {
        final bol = element.split('-');
        if (bol[0] == _prefs.username) {
          boletosModel.username = bol[0];
          boletosModel.title = bol[1];
          boletosModel.coverURL = bol[2];
          boletosModel.posterURL = bol[3];
          boletosModel.horario = bol[4];
          boletosModel.boletos = bol[5];
          boletosModel.asientos = bol[6];
          res.add(boletosModel);
        }
      });
    }

    return res;
  }
}
