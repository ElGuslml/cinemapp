import 'package:cinemapp2/src/pages/pelicula_detalle.dart';
import 'package:cinemapp2/src/routes/routes.dart';
import 'package:cinemapp2/src/share_prefs/preferencias_usuario.dart';
import 'package:flutter/material.dart';

import 'package:cinemapp2/src/pages/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CinemApp',
      initialRoute: 'home',
      routes: getApplicationRoutes(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(builder: (BuildContext context) => HomePage());
      },
    );
  }
}
